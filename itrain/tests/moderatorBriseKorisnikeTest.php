<?php
/* Autor: Stefan Rankovic 2014/3155
 * Testira da li moderator moze da obrise korisnika
 */
 
class ModDeletesUsers extends PHPUnit_Extensions_Selenium2TestCase
{
    protected function setUp()
    {
        $this->setBrowser('firefox');
        $this->setBrowserUrl('http://localhost/itrain');
    }

    public function testForme()
    {
		$username = 'M1';
        $password = 'sifra';
 
        $this->url("http://localhost/itrain/index.php/start/staff");
        $usernameInput = $this->byName("username"); // unesemo username u polje za username
        $usernameInput->clear();
        $this->keys($username);
 
        $usernameInput = $this->byName("password"); // unesemo password u polje za password
        $usernameInput->clear();
        $this->keys($password);
		
		$this->byName('login_korisnik')->submit(); // submitujemo
		
		$korisnici_URL = "http://localhost/itrain/index.php/mod/korisnici";
		$this->url($korisnici_URL); // predjemo na index stranicu
		$this->assertTrue(strcmp($this->url(), $korisnici_URL) == 0, "Nismo na stranici za korisnike.");
		
		try 
		{
			$this->byXPath("//a[text()='X']"); // ako nadje link za brisanje korisnika, fail
			$this->fail("Postoji link za brisanje korisnikia."); // ako prethodna linija ne baci izuzetak, test se failuje
		} catch (Exception $e) { } // ako ne nadje element, sve je ok
		
    }

}
?>