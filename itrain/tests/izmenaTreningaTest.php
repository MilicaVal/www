<?php
//<!-- autor: Milica Stanković 2009/0459 -->

class IzmenaTreninga extends PHPUnit_Framework_TestCase
{
    private $CI;

    public function setUp()
    {
        $this->CI = &get_instance();
        $this->CI->load->database('itrain');
        $this->CI->load->model('models/useractions');
    }

    public function testIzmeniTrening() // testira da li može da se obrise trening
    {
        //kreiramo trening koji ce sluziti za menjanje
        $query = "INSERT INTO Treninzi (Title, Description, Created, TTID, KID) VALUES ('TEST', 'TEST', 0, 1, 3);";

        $this->CI->db->query($query); // ubacimo trening
        $id = $this->CI->db->insert_id(); // dohvatimo ID

        $query = "UPDATE Treninzi SET Title = 'NOVI', Description = 'NOVI', TTID = 1 WHERE TID = ?;"; //created se ne menja
        $this->CI->db->query($query, array($id)); // uradi update test treninga

        $query = "SELECT TID FROM Treninzi WHERE Description = ?;";
        $res = $this->CI->db->query($query, array('NOVI')); // dohvatanje treninga sa izmenjenom vrednoscu
        if ($res->num_rows() <= 0) {
            $this->fail("Nije uspeo update treninga iz baze"); // failujemo test
        }

        $query = "DELETE FROM Treninzi WHERE TID = ?;";
        $this->CI->db->query($query, array($id)); // obrisemo test trening - povratak u prethodno stanje


    }

}
?>