<?php
/* Autor: Stefan Rankovic 2014/3155
 * Testira da li moderator moze da vidi i pristupi stranici za upravljanje osobljem
 */
 
class ModMenu extends PHPUnit_Extensions_Selenium2TestCase
{
    protected function setUp()
    {
        $this->setBrowser('firefox');
        $this->setBrowserUrl('http://localhost/itrain');
    }

    public function testForme()
    {
		$username = 'M1';
        $password = 'sifra';
 
        $this->url("http://localhost/itrain/index.php/start/staff");
        $usernameInput = $this->byName("username"); // unesemo username u polje za username
        $usernameInput->clear();
        $this->keys($username);
 
        $usernameInput = $this->byName("password"); // unesemo password u polje za password
        $usernameInput->clear();
        $this->keys($password);
		
		$this->byName('login_korisnik')->submit(); // submitujemo
		
		$index_URL = "http://localhost/itrain/index.php/mod/index";
		$this->url($index_URL); // predjemo na index stranicu
		$this->assertTrue(strcmp($this->url(), $index_URL) == 0, "Nismo se uspesno ulogovali.");

		try 
		{
			$this->byXPath("//a[text()='Osoblje']"); // ako nadje link za stranicu za osoblje, failuje
			$this->fail("Postoji link ka stranici za osoblje"); // ako prethodna linija ne baci izuzetak, test se failuje
		} catch (Exception $e) { } // ako ne nadje element, sve je ok
		
		
		$osoblje_URL = "http://localhost/itrain/index.php/admin/osoblje";
		$this->url($osoblje_URL); // predjemo na URL za manipulaciju osobljem
		$this->assertFalse(strcmp($this->url(), $osoblje_URL) == 0, "Nekako smo dospeli na stranicu za osoblje."); // proverimo da li je prelazak uspeo
		
    }

}
?>