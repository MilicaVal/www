<?php
/* Autor: Milica Stankovic 2009/0459
 * Testira da li korisnik moze da izabere tip treninga. Ovo ne bi smelo da se desava jer samo treneri mogu da biraju tip treninga.
 */
 
class GUIKOrisnikBiraTip extends PHPUnit_Extensions_Selenium2TestCase
{
    private $CI;
	
	protected function setUp()
    {
        $this->setBrowser('firefox');
        $this->setBrowserUrl('http://localhost/itrain');
		$this->CI = &get_instance();
		$this->CI->load->database('itrain');
    }

    public function testForme()
    {
		// LOGIN KORISNIKA
        $username = 'K2';
        $password = 'sifra';
 
        $this->url("http://localhost/itrain/");
        $usernameInput = $this->byName("username"); // unesemo username u polje za username
        $usernameInput->clear();
        $this->keys($username);
 
        $usernameInput = $this->byName("password"); // unesemo password u polje za password
        $usernameInput->clear();
        $this->keys($password);
		
		$this->byName('login_korisnik')->submit(); // submitujemo

        // FORMA ZA NOVI TRENING
		$index_URL = "http://localhost/itrain/index.php/korisnik/novi";
		$this->url($index_URL); // predjemo na stranicu za kreiranje novog treninga

		try 
		{
			$selektor = $this->elements($this->using('css selector')->value('*[name="trtip"]')); // ne bi smeo ni jedan select element da postoji na ovoj strani
			$this->assertEquals(0, count($selektor)); // ne sme da postoji element sa imenom trtip
		}
		catch (Exception $e) 
		{ 
			$this->fail("Nije dohvacen korisnikov trening");
		} 

    }

}
?>