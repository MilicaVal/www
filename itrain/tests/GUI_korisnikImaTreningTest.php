<?php
/* Autor: Milica Stankovic 2009/0459
 * Testira da li korisnik moze da pregleda trening preko forme
 */
 
class GUI_korisnikImaTreningTest extends PHPUnit_Extensions_Selenium2TestCase
{
	private $CI;
	
    protected function setUp()
    {
        $this->setBrowser('firefox');
        $this->setBrowserUrl('http://localhost/itrain');
		$this->CI = &get_instance();
		$this->CI->load->database('itrain');
    }

    public function testForme()
    {
	
		// PRAVLJENJE TRENINGA ZA IZMENU

        $query = "INSERT INTO Treninzi (Title, Description, Created, TTID, KID) VALUES ('TEST', 'TEST♥♦♣♠♥♦♣♠', 0, 1, 3);";

        $this->CI->db->query($query); // ubacimo trening
        $id = $this->CI->db->insert_id(); // dohvatimo ID
		
		// LOGIN KORISNIKA
        $username = 'K2';
        $password = 'sifra';
 
        $this->url("http://localhost/itrain/");
        $usernameInput = $this->byName("username"); // unesemo username u polje za username
        $usernameInput->clear();
        $this->keys($username);
 
        $usernameInput = $this->byName("password"); // unesemo password u polje za password
        $usernameInput->clear();
        $this->keys($password);
		
		$this->byName('login_korisnik')->submit(); // submitujemo

        // FORMA ZA PREGLED KORISNIKOVIH TRENINGA
		$index_URL = "http://localhost/itrain/index.php/korisnik/moji";
		$this->url($index_URL); // predjemo na stranicu za pregled kreiranog treninga

		try 
		{
			$t_page = "http://localhost/itrain/index.php/korisnik/pregledaj/" . $id; // stranica sa treningom
			$this->url($t_page); // prelazimo na stranicu sa treningom
			$this->assertTrue(strcmp($this->url(), $t_page) == 0, "Nismo na stranici za pregled treninga.");
		}
		catch (Exception $e) 
		{ 
			$this->fail("Nije dohvacen korisnikov trening");
		} 
		
		// UNDO PROMENA U BAZI

        $query = "DELETE FROM Treninzi WHERE Description = 'TEST♥♦♣♠♥♦♣♠';";
        $this->CI->db->query($query, array($id)); // obrisemo test trening - povratak u prethodno stanje

    }

}
?>