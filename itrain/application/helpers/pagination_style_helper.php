<?php
function page_style() { // stilizuje config pri paginaciji
    $config['first_tag_open'] = $config['last_tag_open']=
    $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = ' &nbsp; ';

    $config['first_tag_close'] = $config['last_tag_close']=
    $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = ' &nbsp; ';

    $config['first_link'] = 'First';
    $config['last_link'] = 'Last';

    $config['cur_tag_open'] = " [ ";
    $config['cur_tag_close'] = " ] ";
	return $config;
}
?>
