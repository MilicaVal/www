<!-- autor: Milica Stanković 2009/0459 -->


<div class="menu" name="usermenu" title="Staff meni">
    <?php
    $this->load->helper('url');
    $this->load->library('session');
    $CI =& get_instance();
    $tip = $CI->session->userdata('typestring');
    ?>
    <ul class='dropdown'>
        <li id="top"><a href=<?php echo site_url($tip.'/index'); ?>>Staff panel</a>
            <span></span>
            <ul class="dropdown-box">

                <li><a href=<?php echo site_url($tip.'/treninzi'); ?>>Treninzi</a></li>
                <li><a href=<?php echo site_url($tip.'/korisnici'); ?>>Korisnici</a></li>
                <?php
                    if (strcmp($tip, 'admin')==0){
                       echo "<li>";
                        echo "<a href='";
                        echo site_url($tip.'/osoblje');
                        echo "'>Osoblje</a></li>";

                        echo "</li>";
                }
                ?>
                <li><a href=<?php echo site_url('logout/logout'); ?>>Odjava</a></li>
            </ul>
        </li>
    </ul>
</div>