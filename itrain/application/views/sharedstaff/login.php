<!-- autor: Milica Stanković 2009/0459 -->
<!-- autor: Stefan Ranković, 2014/3155 -->



<h1>Unesite korisničko ime i šifru:</h1>

<?php
$this->load->helper('form_helper'); // treba nam form helper
$this->load->library('table'); // pomoc kod kreiranja tabele

echo form_open("adminlogin/login", array("method"=>"post")); // otvorimo formu, tip je post
$CI =& get_instance(); // dohvatimo instancu codeignitera (jer sledece linije ne rade preko $this)
$CI->table->add_row(form_label("Korisničko ime:", "username"), form_input('username', ''));
$CI->table->add_row(form_label("Lozinka:", "password"), form_password('password', ''));
$CI->table->add_row(form_submit('login_korisnik', 'Loguj se'));
echo $CI->table->generate(); // napravimo tabelu
echo form_close(); // zatvorimo formu
?>
