<!-- autor: Milica Stanković 2009/0459 -->



<?php
/**
 * Created by PhpStorm.
 * User: Milica
 * Date: 24.5.2015.
 * Time: 18:32
 */

$this->load->library('table'); // pomoc kod kreiranja tabele
$this->load->library('session');
$CI =& get_instance(); // dohvatimo instancu codeignitera (jer sledece linije ne rade preko $this)
$type = $CI->session->userdata('type'); // dohvatimo tip korisnika
$heading = array('ID','Korisnik', 'Tip','Promoviši', 'Email', 'Blokiran', 'Promeni');
if ($type == UserTypes::Admin) {
	$heading[] = 'Briši';
}
$CI->table->set_heading($heading); // postavimo heading
$usertype = $CI->session->userdata('typestring');

foreach ($users as $u) { // PAŽNJA: radi sa redovima iz baze
	$row = array (
		$u->getKID(),
        $u->getUsername(),
        ($u->getType())==1 ? "Regularni korisnik" : "Trener",
        ($u->getType())==1 ? anchor($usertype . '/unapredi/'.$u->getKID(), '○'): '', //postavljanje trenera
        $u->getEmail(),
        ($u->getBlocked())? "blokiran" : "",
        ($u->getBlocked())? anchor($usertype . '/odblokiraj/'.$u->getKID(), 'dozvoli') :
        anchor($usertype . '/blokiraj/'.$u->getKID(), '▬')
	);
	if ($type == UserTypes::Admin) {
		$row[] = anchor($usertype . '/obrisi_korisnika/'.$u->getKID(), 'X');
	}
    $CI->table->add_row(
        $row
    );
}
echo $CI->table->generate(); // napravimo tabelu
echo $CI->pagination->create_links(); // napravimo linkove za paginaciju

?>

</br><div class=system>1) nije dozvoljeno redukovati prava trenera.</div>
<div class=system>2) sve preduzete akcije su trajne izmene u bazi.</div>
<?php if ($type == UserTypes::Admin) { ?>
<div class=system>3) pri brisanju korisničkog naloga, brišu se svi autorski treninzi.</div>
<?php } ?>	
	