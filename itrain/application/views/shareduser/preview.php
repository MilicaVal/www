


<link rel="stylesheet" href="<?php echo base_url();?>/public/css/dragdrop_style_editor.css" type="text/css" media="screen"/>

<!-- tables inside this DIV could contain drag-able content  -->
<div id="redips-drag" style="float: left;" >


    <!-- PHP SEGMENT: TRAINING HEADER -->

                    <?php
                    // php parametri za punjenje tabele - info o treningu

                    $trening_naslov = $training->getTitle();
                    $trening_opis =  $training->getDescription();
                    $trening_kreiran = $training->getCreated();
                    /*$trening_autor = $training->getAuthor();*/
                    // TODO: Stefan da napiše u treningu funkciju za dohvatanje imena autora

                    ?>

    <table id="tbl1">
        <colgroup>
            <col width="30"/>
            <col width="100"/>
        </colgroup>
        <tbody>
        <tr>
            <th colspan="5" class="redips-mark"><?php echo $trening_naslov;?></th>
        </tr>
        <tr>
            <th class="redips-mark"></th>
            <th class="redips-mark">Blok</th>
            <th class="redips-mark">Opis</th>
            <th class="redips-mark">Trajanje</th>

        </tr>
                    <?php
                    foreach ($training->getBlocks() as $block) {

                        $naslov = $block->getTitle();
                        $opis = $block->getDescription();
                        $trajanje = $block->getDuration();

                        echo '<tr class="rl">';
                        echo '<td class="redips-rowhandler"><div class="redips-drag redips-row"></div></td><td class="cdark"><div class="redips-drag blue">';
                        echo '<input readonly class="polje" type="text" name="naziv" value="';
                        echo $naslov;
                        echo '"></div></td>';
                                //input za naziv bloka
                        echo '<td class="opis">';
                        echo '<textarea readonly class="polje"  type="" name="opis" cols="10" >';
                        echo $opis;
                        echo '</textarea></td>';
                                //textarea za opis treninga
                        echo '<td class="trajanje"><input readonly class="polje" type="text" name="trajanje" value="';
                        echo $trajanje;
                        echo '"></td></tr>';

                    }
                    echo '<tr><td /><td /><td colspan=2 class="trajanje">Ukupno trajanje: ';
                    $t=$training->totalDuration();
                    $hr=floor($t/60);
                    $min=round($t-$hr*60,0);
                    echo $hr, " sati, ",$min," minuta.";
                    echo '</td></tr>';
                    echo '<tr><td colspan="6" class="redips-mark">';
                    echo $trening_opis,'<br />Kreiran:',$trening_kreiran;
                    echo '</td></tr>';
                    echo '</tbody></table>';
                    ?>

                    <?php
                    $this->load->library('session');
                    $CI =& get_instance();
                    if($CI->session->userdata("KID") == $training->getOwnerID()) { // ako je ovo moj trening, ponuditi opciju za izmenu
                        $this->load->helper('form_helper');
                        echo form_open($CI->session->userdata('typestring') . '/izmeni/' . $training->getTID(), array("method"=>"post")); // forma za izmenu treninga
                        echo form_submit('', 'Izmeni'); // samo sadrzi jedno dugme
                        echo form_close();
                    }
                    echo '</div>';
                    echo ' <br /><a href="javascript:javascript:history.go(-1)">Povratak na prethodnu stranu</a>';
                    ?>



