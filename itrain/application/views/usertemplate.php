<!-- autor: Stefan Ranković, 2014/3155 -->

<?php
$this->load->view('header', array('title'=>'iTrain | ' . $title));
$this->load->view('menu/userMenu');
?>

<!-- start: Content-->
<div id="content" name="myContent" title="Sadrzaj" style=" border-color: red" float="right" >

<?php
$this->load->view($body);
?>

<!-- end: Content-->
	<div class="push"></div>
</div>

<?php
$this->load->view('footer');
?>
