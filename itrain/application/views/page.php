<!-- autor: Milica Stanković 2009/0459 -->

<!-- GLOBALNI TEMPLATE ZA STRANICE

/**
 * Created by PhpStorm.
 * User: Milica
 * Date: 21.5.2015.
 * Time: 14:10
 */ -->

<!--
 * Created by PhpStorm.
 * User: Milica
 * Date: 16.5.2015.
 * Time: 23:55
 */-->

<!-- PARAMETRI: $title, $menu, $body -->
<!doctype html>
<html>
<head>

    <title><?php echo $title;?></title>

    <!-- CSS-->
    <link rel="stylesheet" href="<?php echo base_url();?>public/css/default" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>public/css/globalMenu_style" type="text/css">
    <!-- <link rel="stylesheet" href="public/css/default" type="text/css"> */-->

</head>
<body background="<?php echo base_url();?>public/images/homebackground.png">

<!-- start: Wrap-->
<div id="wrap">

    <!-- start: Header-->
    <div id="header" style=" float:right;" >
        <!-- HEADER of HEADER-->
        <div id="socialstrip" style=" float:right; position: relative">
            <a href="https://www.facebook.com/"><img src="<?php echo base_url();?>public/images/facebook.png" width="36" height="36"></a>
            <a href="https://www.linkedin.com/"><img src="<?php echo base_url();?>public/images/linkedin.png" width="36" height="36"></a>
            <a href="https://plus.google.com/"><img src="<?php echo base_url();?>public/images/google.png" width="36" height="36"></a>
            <a href="https://twitter.com/"><img src="<?php echo base_url();?>public/images/twitter.png" width="36" height="36"></a>
        </div>
        <img src="<?php echo base_url();?>public/images/header.png" width="99%">
        <?php $this->load->view('home/userStrip');?>
    </div>

    <!-- start: Menu-->
    <div id="menu" style=" float:left;">
        <?php $this->load->view('menu/globalMenu');?>
        <?php
                $this->load->view($menu);
        ?>

    </div>
    <!-- end: Menu-->

    <!-- start: Content-->
    <div id="content" name="myContent" title="Test Title" style=" float:left;width:80%;">
            <?php $this->load->view($body);?>
    <!-- end: Content-->
    </div>

    <!-- start: Footer-->
    <div id="footer" style=" float:left;width:99%;text-align: center">
        <p>
            <a target="_blank" href="http://si3psi.etf.rs/">Principi Softverskog Inženjerstva</a> |
            <a href="<?php echo base_url();?>index.php/start/about">Tim: iTrain</a> |
            <a href="<?php echo base_url();?>">Projekat: Trening Sistem</a> |
            maj 2015.
        </p>
    </div>
    <!-- end: Footer-->

    <!-- end: Wrap-->
</div>

</body>