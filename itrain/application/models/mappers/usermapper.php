<!-- autor: Milica Stanković 2009/0459 -->
<!-- autor: Stefan Ranković, 2014/3155 -->

<?php
include_once('/../entity/user.php');

class UserMapper extends CI_Model {

	public function __construct() {
		$this->load->database();
	}
	
	// KONVERTER
	
	private static function load_row($row) { // iz reda dohvacenog sa row() sastavlja User objekat
		$user = new User(); 
		$user->setKID($row->KID); // olaksava pretragu, bice deo sesije
		$user->setUsername($row->Username);
		$user->setPassword($row->Password);
		$user->setEmail($row->Email);
		$user->setBlocked($row->Blocked);
		$user->setType($row->TKID);
		return $user;
	}
	
	// FINDERI
	
	public function find($username) { // Pretrazuje korisnike po korisnickom imenu, vraca nadjenog korisnika ili null, ako nema
		$query = "SELECT * FROM Korisnici WHERE Username = ?;";
		$res = $this->db->query($query, $username);
		$user = null;
		if ($res->num_rows() > 0) {
			$row = $res->row();
			$user = UserMapper::load_row($row); // sastavimo user objekat od informacija dohvacenih iz baze
		}
		return $user;
	}
	
	public function findAllType($offset, $limit, $type) { // nalazi $limit redova, pocevsi od $offset, $type govori kog tipa korisnike nalazimo
		$query = "SELECT * FROM Korisnici WHERE TKID = ? LIMIT ?, ?;";
		$res = $this->db->query($query, array($type, $offset, $limit));
		$users = array();
		foreach ($res->result() as $row) { // sve sto je pronadjeno konvertuje u User objekte i stavlja u niz $users
			$users[] = UserMapper::load_row($row);
		}
		return $users; // vracamo niz dohvacenih Usera
	}

    public function findAllStaff() { // vratice redove, ne konkretne objekte
        $query = "SELECT * FROM Korisnici WHERE TKID=3 OR TKID=4 ORDER BY TKID DESC ;"; // vratice samo Username
        $res = $this->db->query($query);
        $users = array();
        foreach ($res->result() as $row) {
            $users[] = UserMapper::load_row($row); // stavljamo ih u niz
        }
        return $users;
    }
	
	public function findSomeUsers($offset, $limit) { // vratice korisnike i trenere
        $query = "SELECT * FROM Korisnici WHERE TKID=1 OR TKID=2 ORDER BY TKID DESC LIMIT ?, ?;";

        $res = $this->db->query($query, array($offset, $limit));
        $users = array();
        foreach ($res->result() as $row) {
            $users[] = UserMapper::load_row($row); // loadujemo i stavljamo ih u niz
        }
        return $users;
    }
	
	public function findAll($offset, $limit) { // nalazi $limit redova, pocevsi od $offset
		$query = "SELECT * FROM Korisnici LIMIT ?, ?;";
		$res = $this->db->query($query, array($offset, $limit));
		$users = array();
		foreach ($res->result() as $row) { // sve sto je pronadjeno konvertuje u User objekte i stavlja u niz $users
			$users[] = UserMapper::load_row($row);
		}
		return $users; // vracamo niz dohvacenih Usera
	}
	
	// COUNTERI
	
	public function countAll() { // vraca broj ulaza u tabeli korisnika
		return $this->db->count_all_results('Korisnici');
	}
	
	public function countAllType($type) { // vraca broj ulaza u tabeli korisnika za dati tip
		return $this->db->where(array('TKID' => $type))->count_all_results('Korisnici');
	}
	
	public function countAllUsers() { // svi rk i treneri
		$where = "TKID=1 OR TKID=2";
		return $this->db->where($where)->count_all_results('Korisnici');
	}
	
	public function countAllStaff() { // svi moderatori i administratori
		$where = "TKID=3 OR TKID=4";
		return $this->db->where($where)->count_all_results('Korisnici');
	}
	
	
	// CRUD
	
	public function store($user) { // $user je objekat koji predstavlja korisnika
		$check = "SELECT * FROM Korisnici WHERE Username = ?;"; // prvo proveravamo da li je korisnicko ime zauzeto
		$res = $this->db->query($check, $user->getUsername());
		if ($res->num_rows() > 0) {
			throw new Exception("Username ".$user->getUsername()." is taken."); // ime je zauzeto
		}
		$query = "INSERT INTO Korisnici (Username, Password, Email, Blocked, TKID) VALUES (?, ?, ?, ?, ?);"; // upis, ako nismo bacili exception gore
		$this->db->query($query, array($user->getUsername(), $user->getPassword(), $user->getEmail(), $user->getBlocked(), $user->getType()));
	}
	
	public function delete($user) { // brise korisnika sa datim username
		$query = "DELETE FROM Korisnici WHERE Username = ?;";
		$this->db->query($query, $user->username);
	}

    public function delete_user($UID) { // brise korisnika sa datim username
        $query = "DELETE FROM Korisnici WHERE KID = ?;";
        $this->db->query($query, $UID);
    }

    public function promote_user($UID) { // pretvara u trenera korisnika sa UID
        $query = "UPDATE Korisnici SET TKID=2 WHERE KID=?;";
        $this->db->query($query, $UID);
    }

    public function block_user($UID) { // pretvara u trenera korisnika sa UID
        $query = "UPDATE Korisnici SET blocked=1 WHERE KID=?;";
        $this->db->query($query, $UID);
    }

    public function unblock_user($UID) { // pretvara u trenera korisnika sa UID
        $query = "UPDATE Korisnici SET blocked=0 WHERE KID=?;";
        $this->db->query($query, $UID);
    }

	public function update($user) { // updejtuje usera
		$query = "UPDATE Korisnici SET Password = ?, Email = ?, Blocked = ?, TKID = ? WHERE Username = ?;";
		$this->db->query($query, array($user->getPassword(), $user->getEmail(), $user->getBlocked(), $user->getType(), $user->getUsername()));
	}
}
?>
