<!-- autor: Stefan Ranković, 2014/3155 -->

<?php
include_once('/../entity/block.php');

class BlockMapper extends CI_Model {	
	public function __construct() {
		$this->load->database();
	}
	
	// KONVERTER
	
	private static function load_row($row) { // iz reda dohvacenog sa row() sastavlja Block objekat
		$block = new Block(); 
		$block->setBID($row->BID);
		$block->setTitle($row->Title);
		$block->setDescription($row->Description);
		$block->setBlockNum($row->BlockNum);
		$block->setDuration($row->Duration);
		$block->setTrainingID($row->TID);
		return $block;
	}
	
	// FINDERI
	
	public function find($TID) { // Pronacice sve blokove za trening TID, ili ce vratiti prazan array
		$query = "SELECT * FROM Blokovi WHERE TID = ?;";
		$res = $this->db->query($query, $TID);
		$blocks = array();
		foreach ($res->result() as $row) { // pravi Block objekte i puni niz
			$blocks[] = BlockMapper::load_row($row);
		}
		return $blocks; // vracamo niz dohvacenih blokova
	}
	
	public function findOwner($BID) { // vraca ko je vlasnik bloka
		$query = "SELECT KID FROM Treninzi JOIN Blokovi WHERE Blokovi.BID = ?;";
		$res = $this->db->query($query, $BID);
		if ($res->num_rows() > 0) {
			$row = $res->row();
			return $row->KID;
		}
		return 0;
	}
	
	// COUNTERI
	
	public function countBlocksFor($TID) { // vraca broj ulaza u tabeli blokova za trening TID
		return $this->db->where(array('TID' => $TID))->count_all_results('Blokovi');
	}
	
	// CRUD
	
	public function store($block) { // $block je objekat
		$query = "SELECT * FROM Blokovi WHERE BlockNum = ? AND TID = ?;"; // prvo proverimo da li vec ima blok sa istim BlockNum
		$res = $this->db->query($query, array($block->getBlockNum(), $block->getTrainingID()));
		if ($res->num_rows() > 0) { // ako ima radimo overwrite
			$BID = $res->row()->BID; // dohvatimo BID
			$this->delete($BID); // obrisemo stari blok
		}
		$query = "INSERT INTO Blokovi (Title, Description, BlockNum, Duration, TID) VALUES (?, ?, ?, ?, ?);"; // dodamo novi
		$this->db->query($query, array($block->getTitle(), $block->getDescription(), $block->getBlockNum(), $block->getDuration(), $block->getTrainingID()));
	}
	
	public function delete($BID) { // brise blok sa datim ID
		$query = "DELETE FROM Blokovi WHERE BID = ?;";
		$this->db->query($query, $BID);
	}

	public function update($block) { // updejtuje blok
		$query = "UPDATE Blokovi SET Title = ?, Description = ?, BlockNum = ?, Duration = ?, TID = ? WHERE BID = ?;";
		$this->db->query($query, array($block->getTitle(), $block->getDescription(), $block->getBlockNum(), $block->getDuration(), $block->getTrainingID(), $block->getBID()));
	}
}
?>
