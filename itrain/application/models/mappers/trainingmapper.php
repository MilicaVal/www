<!-- autor: Milica Stanković 2009/0459 -->
<!-- autor: Stefan Ranković, 2014/3155 -->

<?php
include_once('/../entity/block.php');
include_once('/../entity/training.php');
include_once('/../entity/trainingtypes.php');
include_once('blockmapper.php');

class TrainingMapper extends CI_Model {
	static private $bm; // block mapper, inicijalizovan na dnu fajla
	
	public static function init_bm() { TrainingMapper::$bm = new BlockMapper(); }
	
	public function __construct() {
		$this->load->database();
	}
	
	// KONVERTER
	
	private static function load_row($row) { // iz reda dohvacenog sa row() sastavlja Block objekat
		$training = new Training(); 
		$training->setTID($row->TID);
		$training->setTitle($row->Title);
		$training->setDescription($row->Description);
		$training->setCreated($row->Created);
		$training->setType($row->TTID);
		$training->setOwnerID($row->KID);
		$blocks = TrainingMapper::$bm->find($training->getTID()); // pokupimo i sve blokove
		$training->setBlocks($blocks); // i upisemo
		return $training;
	}
	
	// FINDERI
	
	public function findByKID($KID) { // Vraca sve treninge koji pripadaju korisniku $KID
		$query = "SELECT * FROM Treninzi WHERE KID = ? ORDER BY Created DESC;";
		$res = $this->db->query($query, $KID);
		$trainings = array();
		foreach ($res->result() as $row) {
			$training = TrainingMapper::load_row($row); // sastavimo trening objekte
			$trainings[] = $training; // stavljamo ih u niz
		}
		return $trainings; // vraca niz
	}
	
	public function findByTID($TID) { // nalazi 1 javni trening sa TID
		$query = "SELECT * FROM Treninzi WHERE TID = ?"; // ako trening nije javni, vratice prazan red
		$res = $this->db->query($query, $TID);
		$training = null;
		if ($res->num_rows() > 0) {
			$training = TrainingMapper::load_row($res->row());
		}
		return $training;
	}
	
	public function findAllPublic() { // vratice redove, ne konkretne objekte
		$query = "SELECT * FROM Korisnici JOIN Treninzi ON Korisnici.KID = Treninzi.KID WHERE TTID = 1 ORDER BY Created DESC;"; // vratice samo Username
		$res = $this->db->query($query);
		$trainings = array();
		foreach ($res->result() as $row) {
			$trainings[] = $row; // stavljamo ih u niz
		}
		return $trainings;
	}

    public function findAllTrainings() { // vratice redove, ne konkretne objekte
        $query = "SELECT * FROM Korisnici JOIN Treninzi ON Korisnici.KID = Treninzi.KID ORDER BY Created DESC;"; // vratice samo Username
        $res = $this->db->query($query);
        $trainings = array();
        foreach ($res->result() as $row) {
            $trainings[] = $row; // stavljamo ih u niz
        }
        return $trainings;
    }
	
	public function findSomeTrainings($offset, $limit) { // vraca $limit treninga pocevsi od $offset offseta, orderovanih po datumu pravljenja
		$query = "SELECT * FROM Korisnici JOIN Treninzi ON Korisnici.KID = Treninzi.KID ORDER BY Created DESC LIMIT ?, ?;";
		$res = $this->db->query($query, array($offset, $limit));
		$trainings = array();
		foreach ($res->result() as $row) {
			$trainings[] = $row; // stavljamo ih u niz
		}
		return $trainings;
	}
	
	public function findSomePublic($offset, $limit) { // vraca $limit javnih treninga pocevsi od $offset offseta
		$query = "SELECT * FROM Korisnici JOIN Treninzi ON Korisnici.KID = Treninzi.KID WHERE TTID = 1 ORDER BY Created DESC LIMIT ?, ?;";
		$res = $this->db->query($query, array($offset, $limit));
		$trainings = array();
		foreach ($res->result() as $row) {
			$trainings[] = $row; // stavljamo ih u niz
		}
		return $trainings;
	}
	
	public function findOwner($TID) { // vraca ko je vlasnik treninga
		$query = "SELECT KID FROM Treninzi WHERE TID = ?;";
		$res = $this->db->query($query, $TID);
		if ($res->num_rows() > 0) {
			$row = $res->row();
			return $row->KID;
		}
		return 0;
	}
	
	// COUNTERI
	
	public function countTrainingsFor($KID) { // vraca broj ulaza u tabeli blokova za trening TID
		return $this->db->where(array('KID' => $KID))->count_all_results('Treninzi');
	}
	
	public function countPublicTrainings() { // vraca broj javnih treninga
		return $this->db->where(array('TTID' => TrainingTypes::PublicTraining))->count_all_results('Treninzi');
	}
	
	public function countAllTrainings() { // vraca broj svih treninga
		return $this->db->count_all_results('Treninzi');
	}
	
	// CRUD
	
	public function store($training) { // $training je objekat
		$query = "INSERT INTO Treninzi (Title, Description, Created, TTID, KID) VALUES (?, ?, ?, ?, ?);"; // dodamo novi
		$this->db->query($query, array($training->getTitle(), $training->getDescription(), $training->getCreated(), $training->getType(), $training->getOwnerID()));
		$TID = $this->db->insert_id(); // dohvatimo ID
		$blocks = $training->getBlocks();
		foreach ($blocks as $block) {
			$block->setTrainingID($TID); // postavimo ownera
			TrainingMapper::$bm->store($block);
		}
		$training->setTID($TID); // updejtujemo ovo polje
	}
	
	public function delete($TID) { // brise trening sa datim ID
		$query = "DELETE FROM Treninzi WHERE TID = ?;";
		$this->db->query($query, $TID);
		$query = "DELETE FROM Blokovi WHERE TID = ?;"; // obrisemo i blokove
		$this->db->query($query, $TID);
	}


	public function update($training) { // updejtuje trening
		$query = "UPDATE Treninzi SET Title = ?, Description = ?, TTID = ? WHERE TID = ?;"; // created necu dirati, kad je napravljen, napravljen je
		$this->db->query($query, array($training->getTitle(), $training->getDescription(), $training->getType(), $training->getTID()));
		$query = "DELETE FROM Blokovi WHERE TID = ?;"; // obrisemo blokove, ubacicemo nove
		$TID = $training->getTID();
		$this->db->query($query, $TID);
		foreach ($training->getBlocks() as $block) {
			$block->setTrainingID($TID); // postavimo ownera
			TrainingMapper::$bm->store($block);
		}
	}
}

TrainingMapper::init_bm();
?>
