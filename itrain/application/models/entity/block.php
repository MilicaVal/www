<!-- autor: Stefan Ranković, 2014/3155 -->

<?php
class Block {
	private $BID;
	private $title;
	private $description;
	private $blockNum;
	private $duration;
	private $trainingID;
	
	public function __construct() {}
	
	public function setBID($BID) { $this->BID = $BID; }
	public function getBID() { return $this->BID; }
	
	public function setTitle($title) { $this->title = $title; }
	public function getTitle() { return $this->title; }
	
	public function setDescription($description) { $this->description = $description; }
	public function getDescription() { return $this->description; }
	
	public function setBlockNum($blockNum) { $this->blockNum = $blockNum; }
	public function getBlockNum() { return $this->blockNum; }
	
	public function setDuration($duration) { $this->duration = $duration; }
	public function getDuration() { return $this->duration; }
	
	public function setTrainingID($trainingID) { $this->trainingID = $trainingID; }
	public function getTrainingID() { return $this->trainingID; }
	
	public function __toString()
    {
        return $this->BID . " " . $this->title . " " . $this->description . " " . $this->blockNum . " " . $this->duration . " " . $this->trainingID;
    }
}
?>
