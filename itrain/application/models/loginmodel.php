<!-- autor: Stefan Ranković, 2014/3155 -->

<?php

include_once('mappers/usermapper.php');
include_once('entity/user.php');
include_once('entity/usertypes.php');

class LoginModel extends CI_Model {
	private $mapper;
	
	public function WrongUsernameOrPassword() { return 1; }
	public function UserIsBlocked() { return 2; }
	public function LoginSuccessful() { return 3; }
	
	public function __construct() {
        parent::__construct();
		$this->mapper = new UserMapper(); // pravimo mapper
    }
	
	public function loginUser($username, $password) {  // ovo se poziva sa login stranice za korisnike
		return $this->login($username, $password, array(UserTypes::RK, UserTypes::Trener));
	}
	
	public function loginSupervisor($username, $password) {  // ovo se poziva sa login stranice za supervizorske naloge
		return $this->login($username, $password, array(UserTypes::Moderator, UserTypes::Admin));
	}
	
	private function login($username, $password, $types) {
		if (empty($username) || empty($password)) { // param check
			return array('status' => $this->WrongUsernameOrPassword(), 'user' => null);
		}
		$user = $this->mapper->find($username);
		if ($user == null || !in_array($user->getType(), $types) || !password_verify($password, $user->getPassword())) {
			$result = array('status' => $this->WrongUsernameOrPassword(), 'user' => null); // pogresno korisnicko ime ili sifra
		} else if ($user->getBlocked()) {
			$result = array('status' => $this->UserIsBlocked(), 'user' => null); // korisnik je blokiran
		} else {
			$result = array('status' => $this->LoginSuccessful(), 'user' => $user); // sve je ok
		}
		return $result; // vraca par rezultat operacije (nepostojeci, blokiran ili uspesno) - korisnicki objekat (ili null za neuspesan login)
	}
}
?>
