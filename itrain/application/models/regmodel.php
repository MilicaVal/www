<!-- autor: Stefan Ranković, 2014/3155 -->
<?php

include_once('mappers/usermapper.php');
include_once('entity/user.php');
include_once('entity/usertypes.php');

class RegModel extends CI_Model {
	private $mapper;
	
	public function __construct() {
        parent::__construct();
		$this->mapper = new UserMapper(); // pravimo mapper

    }
	
	public function register($user) { // prihvata korisnicki objekat i registruje ga u bazi
		try {
			$user->setPassword(password_hash($user->getPassword(), PASSWORD_DEFAULT)); // hashujemo sifru
			$this->mapper->store($user); // baca exception ako je username zauzet

			$success = true;

		} catch (Exception $e) {
			$success = false;
		}
		return $success;
	}
}
?>
