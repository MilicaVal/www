<!-- autor: Stefan Ranković, 2014/3155 -->

<?php
abstract class BaseLogin extends CI_Controller {	
	public function __construct() {
		parent::__construct();
		$this->load->model('loginmodel');
		$this->load->library('session');
		$this->load->helper('url');
	}

	public function login() {
		$success = false;
		$username = $this->input->post('username'); // uzmemo username i password
		$password = $this->input->post('password');
		$result = $this->performLogin($username, $password); // pokusamo login
		if ($result['status'] == $this->loginmodel->LoginSuccessful()) { // uspesan login, cuvamo sta nam treba od sesionih podataka
			$this->session->set_userdata(array('KID' => $result['user']->getKID()));
			$this->session->set_userdata(array('username' => $result['user']->getUsername()));
			$this->session->set_userdata(array('email' => $result['user']->getEmail()));
			$this->session->set_userdata(array('type' => $result['user']->getType()));
			switch ($result['user']->getType()) {
				case UserTypes::RK:
					$this->session->set_userdata(array('typestring' => 'korisnik'));
					break;
				case UserTypes::Trener:
					$this->session->set_userdata(array('typestring' => 'trener'));
					break;
				case UserTypes::Moderator:
					$this->session->set_userdata(array('typestring' => 'mod'));
					break;
				case UserTypes::Admin:
					$this->session->set_userdata(array('typestring' => 'admin'));
					break;
				default:
					die(); // neko hakerisanje, prekinuti izvrsavanje
			}
			redirect($this->session->userdata('typestring') . '/index/', 'refresh'); // ucitavamo polazni view
		} else if ($result['status'] == $this->loginmodel->UserIsBlocked()){
            $this->load->view('errors/html/error', array(
                  'title' => 'Blokiran',
                'heading' => 'Greška: korisnik je blokiran',
                'message' => 'Poštovani,<br />Naše osoblje je blokiralo korisnika sa ovim imenom. Ukoliko mislite da je došlo do
                greške i želite ponovnu aktivaciju naloga, kontaktirajte nas na <a>email</a>.'
            ));
		} else {
			// stranica sa porukom korisniku da je promasio sifru ili username
            $this->load->view('errors/html/error', array(
                'title' => 'Pogrešni parametri',
                'heading' => 'Greška: pogrešna šifra ili korisničko ime',
                'message' => 'Poštovani,<br />Uneli ste pogrešnu šifru ili korisničko ime. Ukoliko ste ih zaboravili ili želite promenu,
 kontaktirajte nas na <a>email</a>.'
            ));
		}
	}
	
	protected abstract function performLogin($username, $password); // ovo ce klase login i admin login morati da implementiraju
}
?>
