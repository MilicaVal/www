<!-- autor: Stefan Ranković, 2014/3155 -->

<?php
class CTest extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('test/mtest');
	}

	public function test()
	{
		$this->mtest->test();
	}
}
?>
