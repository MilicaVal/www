<!-- autor: Stefan Ranković, 2014/3155 -->

<?php
include_once('baselogin.php');

class Login extends BaseLogin {	
	public function __construct() {
		parent::__construct();
	}
	
	protected function performLogin($username, $password) {
		return $this->loginmodel->loginUser($username, $password); // pokusamo login
	}
}
?>
